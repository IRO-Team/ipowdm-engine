from setuptools import setup

setup_requires = ["wheel"]

setup(name='IPoWDM',
      version='0.1',
      description='',
      author='Nicolas Jara, Hermann Pempelfort',
      author_email='',
      url='',
      packages=['src'],
      #   install_requires=INSTALL_REQUIRES,
      include_package_data=True,
      install_requires=[
          "numpy",
      ],
      )

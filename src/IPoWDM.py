from connection import Connection
from simulator import Simulator


class IPoWDM():

    def __init__(self, networkFile, pathFile, timeOn, rho):
        self.__timeOn = timeOn
        self.__timeOff = (timeOn/rho) - timeOn
        self.__simulator =  Simulator(networkFile, pathFile, timeOn = self.__timeOn, timeOff =  self.__timeOff)
        self.__network = self.__simulator._controller._network
        self.__connectionProbabilityArray = []
        self.__bw = []
        #1 + ((j+k) mod 3);



    def calculateElasticProbability(self):
        linksNumbers = self.__network.getNumberOfLinks()
        nodesNumbers = self.__network.getNumberOfNodes()
        rhoLinks = [] #tamaño de Rho es de linksNumbers
        
        #Init Array Connection Probability
        for i in range(nodesNumbers):
            self.__connectionProbabilityArray.append([])
            for j in range(nodesNumbers):
                self.__connectionProbabilityArray[i].append(None)

        for i in range(len(self.__simulator._controller._connections)):
            self.__connectionProbabilityArray[self.__simulator._controller._connections[i].getSource()][self.__simulator._controller._connections[i].getDestiny()] = self.__simulator._controller._connections[i]

        #Calcular el bloqueo del enlace
        for i in range(linksNumbers):
            rhoLinks = []
            for j in range(len(self.__network.getLink(i).getConnections())):
                rhoLinks.append(self.__network.getLink(i).getConnection(j).getRho())
            self.calculateProbabilityForLink(rhoLinks,i)

        #Calculo probabilidad de bloqueo de conexiones
        for i in range(nodesNumbers):
            for j in range(nodesNumbers):
                temporalProbabiltyConnection = 1
                if (i != j):
                    for k in range(len(self.__connectionProbabilityArray[i][j]._links)):
                        temporalProbabiltyConnection = temporalProbabiltyConnection*(1-self.__connectionProbabilityArray[i][j]._links[k].getProbability())
                    self.__connectionProbabilityArray[i][j]._probability = 1 - temporalProbabiltyConnection
        
        #Calculo de la probabilidad de bloqueo
        probabiltyAccumulated = 0 
        temporalLambdaG = 0
        for i in range(nodesNumbers):
            for j in range(nodesNumbers):
                if (i != j):
                    temporalLambda = 1 / (self.__connectionProbabilityArray[i][j].getTimeOn()+self.__connectionProbabilityArray[i][j].getTimeOff())
                    temporalLambdaG = temporalLambdaG + temporalLambda
                    probabiltyAccumulated = probabiltyAccumulated + temporalLambda*self.__connectionProbabilityArray[i][j]._probability
        networkBlocking = probabiltyAccumulated/temporalLambdaG
        print(networkBlocking)
        

    def calculateProbabilityForLink(self, rhoLinks, linkId):
        linksNumbers = self.__network.getNumberOfLinks()
        nodesNumbers = self.__network.getNumberOfNodes()
        capacityOfLink = self.__network.getLink(linkId).getSlots()
        connectionsOfLink = self.__network.getLink(linkId).getQuantityOfConnections()

        probabilityArray = []
        lambdaArray = []
        for i in range(connectionsOfLink):
            probabilityArray.append([])
            for j in range(capacityOfLink+1):
                probabilityArray[i].append(0.0)
            lambdaArray.append(rhoLinks[i]/self.__network.getLink(linkId).getConnection(i).getTimeOn())
        probabilityAux = []
        for _ in range(capacityOfLink+1):
            probabilityAux.append(0.0)
        for i in range(connectionsOfLink):
            probabilityArray[i][0] = 1
            for j in range(connectionsOfLink):
                if (i != j):
                    tmpbw = 0
                    for k in range(j+1):
                        if (k != i):
                            #BW Tamaño de la solicitud de FSU
                            tmpbw = tmpbw + self.__network.getLink(linkId).getConnection(k).getBandwidth()
                    bw = self.__network.getLink(linkId).getConnection(j).getBandwidth()
                    if (tmpbw > capacityOfLink):
                        tmpbw = capacityOfLink
                    for k in range(tmpbw+1):
                        probabilityAux[k] = (1-rhoLinks[j])*probabilityArray[i][k]
                        if (k - bw >= 0):
                            probabilityAux[k] = probabilityAux[k]+rhoLinks[j]*probabilityArray[i][k-bw]
                    for k in range(tmpbw+1):
                        probabilityArray[i][k] = probabilityAux[k]
            #Normanilzación
            for j in range(capacityOfLink):
                probabilityAux[j] = 0
        num = 0
        den = 0
        for i in range(connectionsOfLink):
            bw = self.__network.getLink(linkId).getConnection(i).getBandwidth()
            sump = 0
            for j in range(capacityOfLink+1-bw,capacityOfLink+1):
                sump = sump +probabilityArray[i][j]
            num = num + lambdaArray[i]*(1-rhoLinks[i])*sump
            sump = 0
            for j in range(capacityOfLink+1):
                sump = sump + probabilityArray[i][j]
            den = den + lambdaArray[i]*(1 - rhoLinks[i])*sump
        #print(num/den)
        self.__network.getLink(linkId).setProbability(num/den)
        
    def calculateElasticProbabilityUpStream(self):
        linksNumbers = self.__network.getNumberOfLinks()
        nodesNumbers = self.__network.getNumberOfNodes()
        rhoLinks = []

        #Init Array Connection Probability
        for i in range(nodesNumbers):
            self.__connectionProbabilityArray.append([])
            for j in range(nodesNumbers):
                self.__connectionProbabilityArray[i].append(None)

        for i in range(len(self.__simulator._controller._connections)):
            self.__connectionProbabilityArray[self.__simulator._controller._connections[i].getSource()][self.__simulator._controller._connections[i].getDestiny()] = self.__simulator._controller._connections[i]
        
        for i in range(linksNumbers):
            rhoLinks.append([])
            for j in range(self.__network.getLink(i).getQuantityOfConnections()):
                rhoLinks[i].append(0)
        #self.orderLinks() #Ordenar enlaces por largo
        propabilityBefore = []
        errorPerConnection = [] 
        for i in range(nodesNumbers):
            propabilityBefore.append([])
            errorPerConnection.append([])
            for j in range(nodesNumbers):
                if (i != j):
                    propabilityBefore[i].append(1)
                else:
                    propabilityBefore[i].append(0)
                errorPerConnection[i].append(0)
        error = 1
        while (error > 0):
            #Calcula el rho debido al efecto de aguas arriba y el bloqueo del enlace.
            for i in range(linksNumbers):
                for j in range(self.__network.getLink(i).getQuantityOfConnections()):
                    temporalB1 = 1 #What is that
                    for k in range(self.__network.getLink(i).getConnection(j).getLinksLength()):
                        link = self.__network.getLink(i).getConnection(j).getLink(k)
                        if (( k>0 ) and (self.__network.getLink(i) == link)):
                            for l in range(k-1):
                                link = self.__network.getLink(i).getConnection(j).getLink(l)
                                temporalB1 = temporalB1*(1-link.getProbability())
                    rho = self.__network.getLink(i).getConnection(j).getRho()
                    rhoLinks[i][j] = temporalB1 * rho
                self.calculateProbabilityForLink(rhoLinks[i],i)
            for i in range(nodesNumbers):
                for j in range(nodesNumbers):
                    temporalProbabilityConnection = 1
                    if (i != j):
                        for k in range(self.__connectionProbabilityArray[i][j].getLinksLength()):
                            temporalProbabilityConnection = temporalProbabilityConnection * (1 - self.__connectionProbabilityArray[i][j].getLink(k).getProbability())
                        self.__connectionProbabilityArray[i][j].setProbability(1 - temporalProbabilityConnection)
                        if (self.__connectionProbabilityArray[i][j].getProbability() < 1):
                            errorPerConnection[i][j] = abs((self.__connectionProbabilityArray[i][j].getProbability()-propabilityBefore[i][j])/propabilityBefore[i][j])
                        else:
                            errorPerConnection[i][j] = 1
                        propabilityBefore[i][j] = self.__connectionProbabilityArray[i][j].getProbability()
            for i in range(nodesNumbers):
                for j in range(nodesNumbers):
                    if (i != j):
                        if (errorPerConnection[i][j] > 0.0001):
                            error = 0
        #Calculo de la probabilidad de bloqueo
        probabiltyAccumulated = 0 
        temporalLambdaG = 0
        for i in range(nodesNumbers):
            for j in range(nodesNumbers):
                if (i != j):
                    temporalLambda = 1 / (self.__connectionProbabilityArray[i][j].getTimeOn()+self.__connectionProbabilityArray[i][j].getTimeOff())
                    temporalLambdaG = temporalLambdaG + temporalLambda
                    probabiltyAccumulated = probabiltyAccumulated + temporalLambda*self.__connectionProbabilityArray[i][j]._probability
        networkBlocking = probabiltyAccumulated/temporalLambdaG
        print(networkBlocking)
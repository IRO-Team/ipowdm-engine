# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 16:25:01 2022

@author: redno
"""

class Connection:
    _id : -1
    _links : None
    _slots : None
    _bandwidth : 0
    _probability : 0.0
    _timeOn : 0.0
    _timeOff : 0.0
    _rho : 0.0
    
    def __init__(self, id, timeOn, timeOff):
        self._id = id
        self._links = []
        self._slots = []
        self._probability = 0.0
        self._timeOn = timeOn
        self._timeOff = timeOff
        self._rho = timeOn/(timeOn+timeOff)
    
    def addLink(self, idLink, slots):
        self._links.append(idLink)
        self._slots.append(slots)
        
    def addLink(self, idLink, fromSlot, toSlot):
        self._links.append(idLink)
        lSlots = []
        for i in range( fromSlot,  toSlot):
            lSlots.append(i)
        self._slots.append(lSlots)

    def getLinks(self):
        return self._links
    
    def getLink(self, id):
        return self._links[id]
    
    def getLinksLength(self):
        return len(self._links)

    def getTimeOn(self):
        return self._timeOn
    
    def getTimeOff(self):
        return self._timeOff

    def getProbability(self):
        return self._probability
    
    def setProbability(self, value):
        self._probability = value

    def setBandwidth(self, bandwidth):
        self._bandwidth = bandwidth
    
    def getBandwidth(self):
        return self._bandwidth
    
    def getSource(self):
        return self._links[0]._src
    
    def getDestiny(self):
        return self._links[len(self._links)-1]._dst

    def getRho(self):
        return self._rho
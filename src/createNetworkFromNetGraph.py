import os
import re
from networkClass import NetworkClass
from linkDetail import LinkDetail
from nodeDetail import NodeDetail

cwd = os.getcwd()  # Get the current working directory (cwd)

fichero = open(cwd+"\\src\\UKNet.top")
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
networkJson = NetworkClass()
for _ in range(0,21):
    nodeDetail = NodeDetail()
    nodeDetail.id = _
    networkJson.nodes.append(nodeDetail)
caracter = fichero.readline()
i = 0
for _ in range(0,78):
    linkDetail = LinkDetail()
    linkDetail.id = i
    i = i + 1
    networkJson.links.append(linkDetail)
while caracter != "":
    name = caracter.rstrip(caracter[-1])
    var = re.split(r'\t+', name.rstrip('\t'))
    networkJson.links[int(var[3])].src = int(var[0])
    networkJson.links[int(var[3])].dst = int(var[1])
    caracter = fichero.readline()
jsonFile = open("UKNet_Netgraph.json", "w")
jsonFile.write(networkJson.toJSON())
jsonFile.close()
#print(routeJson.toJSON())
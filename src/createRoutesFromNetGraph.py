import os
import re
import json
from routeClass import RouteClass
from routeDetail import RouteDetail

cwd = os.getcwd()  # Get the current working directory (cwd)

fichero = open(cwd+"\\src\\UKNet_Baroni.txt")

f = open(cwd+"\\src\\UKNet_Netgraph.json", "r")
  
# returns JSON object as 
# a dictionary
network = json.loads(f.read())
f.close()
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
fichero.readline()
caracter = fichero.readline()
routeJson = RouteClass()
while caracter != "":
    #print(caracter)
    routeDetail = RouteDetail()
    name = caracter.rstrip(caracter[-1])
    routeJson.name = name
    
    var = re.split(r'\t+', name.rstrip('\t'))
    if (len(var) >3):
        routeDetail.src = int(var[0])
        routeDetail.dst = int(var[1])
        res = [eval(i) for i in var[3:]]
        for i in range(len(res)):
            routeDetail.paths.append(network["links"][res[i]]["src"])
        routeDetail.paths.append(network["links"][res[len(res)-1]]["dst"])
        routeJson.routes.append(routeDetail)
    caracter = fichero.readline()
jsonFile = open("UKNet_baroni.json", "w")
jsonFile.write(routeJson.toJSON())
jsonFile.close()
#print(routeJson.toJSON())
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 11:59:20 2022

@author: redno
"""

import enum


class EventType(enum.Enum):
    NoData = 1
    Arrive = 2
    Departure = 3


class Event:
    _eventType = EventType.NoData
    _time = -1
    _idConnection = -1

    def __init__(self, eventType, time, idConnection):
        self._eventType = eventType
        self._time = time
        self._idConnection = idConnection

    def getTime(self):
        return self._time

    def getType(self):
        return self._eventType

    def getIdConnection(self):
        return self._idConnection

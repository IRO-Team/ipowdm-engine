# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 14:37:03 2022

@author: redno
"""


class Link:
    _id = -1
    _length = 1
    _slots = None
    _src = -1
    _dst = -1
    __probability = 0.0

    def __init__(self, id=-1, length=1, slots=1):
        self._id = id
        self._length = length
        self._slots = []
        self.__connections = []
        for x in range(slots):
            self._slots.append(False)
        self._src = -1
        self._dst = -1

    def setSlots(self, slots):
        # Search if slots are used.
        for slot in self._slots:
            if slot == True:
                break

        if slots > len(self._slots):
            for x in range(slots-len(self._slots)):
                self._slots.append(False)

        else:
            for x in range(len(self._slots)-slots):
                del self._slots[0]

    def getSlots(self):
        return len(self._slots)

    def getSlot(self, idSlot):
        return self._slots[idSlot]

    def info(self):
        print("ID: ", self._id)
        print("Ancho", self._length)
        print("Se muestra el estado de los slots")
        for slot in self._slots:
            print(slot)
        print("Fuente: ", self._src, " - Destino: ", self._dst)

    def getConnections(self):
        return self.__connections
    
    def getConnection(self, id):
        return self.__connections[id]
    
    def getQuantityOfConnections(self):
        return len(self.__connections)

    def setProbability(self, probability):
        self.__probability = probability

    def getProbability(self):
        return self.__probability
import json
class NetworkClass():
    def __init__(self):
        self.alias = ""
        self.name = ""
        self.nodes = []
        self.links = []
    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)
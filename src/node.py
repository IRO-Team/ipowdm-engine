# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 14:45:23 2022

@author: redno
"""

class Node:
    _id = -1
    _label = ""
    
    def __init__(self, id, label=""):
        self._id = id
        self._label = label
    
    def info(self):
        print(self._id,self._label)   
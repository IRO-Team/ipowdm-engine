# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 15:10:30 2022

@author: redno
"""

import json
from node import Node
from link import Link


class Reader:
    def readNetwork(self, file, nodes, links):
        with open(file) as json_file:
            info = json.load(json_file)
            for readNode in info['nodes']:
                node = Node(readNode['id'])
                nodes.append(node)
            for readLink in info['links']:
                link = Link(
                    #readLink['id'], readLink['length'], readLink['slots'])
                    readLink['id'], readLink['length'], 16)
                link._src = readLink['src']
                link._dst = readLink['dst']
                links.append(link)



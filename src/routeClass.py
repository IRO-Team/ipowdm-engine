import json

class RouteClass:
    def __init__(self):
        self.alias = ""
        self.name = ""
        self.routes = []

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)
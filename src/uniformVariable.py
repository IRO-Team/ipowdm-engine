# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 12:49:37 2022

@author: redno
"""

from numpy.random import Generator, MT19937, SeedSequence
import numpy as np

from randomVariable import RandomVariable


class UniformVariable():
    # _generator = None
    # _parameter = None
    # _dist = None

    def __init__(self, seed, parameter):
        if (parameter < 0):
            raise ("Parameter 1  must be positive.")
        self._parameter = parameter
        sg = SeedSequence(seed)
        self._generator = Generator(MT19937(sg))
        # self._dist = np.random.uniform(0, parameter, parameter)
        self._dist = self._generator.uniform(0, 1.0)

    def getNextValue(self):
        return self._generator.random()*self._parameter

    def getNextIntValue(self):
        return int(self._generator.random()*self._parameter)
